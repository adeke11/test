<?php

namespace App\Http\Controllers\Api\V1;

use App\Department;
use App\Http\Controllers\Controller;
use App\Staff;
use Illuminate\Http\Request;

class IndexController extends SiteController
{
    public function index(){

        $staff = $this->getStaff();

        $departments = Department::all();

        return response()->json(['status'=> 'true', 'staff' => $staff, 'departments' => $departments], 200);
    }
}
