<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Staff;


class SiteController extends Controller
{

    protected function getStaff(){
        $staff = Staff::with('departments')->get();

        $staff->transform(function($item) {
            $arr = array();

            foreach ($item->departments as $department){
                array_push($arr, $department->pivot->department_id);
            }

            $item->departments_id = $arr;

            return $item;

        });

        return $staff->except('departments');
    }
}
