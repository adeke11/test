<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Staff extends Model
{
    public function departments()
    {
        return $this->belongsToMany('App\Department')->withPivot('department_id');

    }

    static function departments_id($id){

        return DB::table('department_staff as d_s')
            ->where('staff_id', $id)
            ->pluck('d_s.department_id')
            ->toArray();
    }
}
