<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Department extends Model
{
    protected $fillable = ['name'];

    public function staff()
    {
        return $this->belongsToMany('App\Staff')->withPivot('staff_id');
    }

    static function salary($id){

        return DB::table('department_staff as d_s')
            ->select('s.salary')
            ->where('department_id', $id)
            ->leftJoin('staff as s', 's.id', '=', 'd_s.staff_id')
            ->max('s.salary');
    }
}
