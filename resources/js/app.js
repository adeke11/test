require('./bootstrap');
import Vue from 'vue';

import Routes from './routes';

import App from './views/App';

Vue.component('app', require('./views/App.vue'));


const app = new Vue({
   el: '#app',
   router: Routes,
   render: h => h(App),
});

export default app;